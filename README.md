# Skipfile testing

This project is the home for the Gitlab CI pipeline for retesting skipfiles.

# Purpose of the project

The pipeline is used to run tests from the LTP skipfile ([skipfile-lkft.yaml](https://github.com/Linaro/test-definitions/blob/master/automated/linux/ltp/skipfile-lkft.yaml)) and automate updates to this skipfile based on the results of this testing.

# Running the pipeline

To run the pipeline, navigate to the `Build` tab within this project in Gitlab, and click `Run pipeline`.

The parameters `RUN_COUNT` and `PUSH_RESULTS` can be used to specify the number of times the tests should be run and whether or not pull requests should be created in the [test-definitions](https://github.com/Linaro/test-definitions) repo based on the results.
